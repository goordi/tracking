import json
from multiprocessing import Process

import redis
from bottle import BaseRequest, route, run, request, response, BytesIO

import config
from web.utils import analyze_js_stacktrace, find_text_in_region, download_file

BaseRequest.MEMFILE_MAX = 2 << 20  # 2 MB should be enough

_POOL = redis.ConnectionPool(**config.REDIS_PARAMS)
redis_conn = redis.StrictRedis(connection_pool=_POOL)


def process_log_entry(identifier, args):
    source_url = args.get('src')
    source_html = args.get('doc').encode('utf8')
    stacktrace = args.get('stack')

    caller, initiator = analyze_js_stacktrace(stacktrace)
    if caller.file == source_url:
        body = source_html
    else:
        body = download_file(caller.file, BytesIO()).read()

    try:
        body = body.decode('utf8')
    except UnicodeEncodeError:
        RuntimeWarning('Bad file downloaded')
        return False

    script = dict()
    script['url'] = caller.file
    script['line'] = caller.line
    script['char'] = caller.char
    by_line = body.split('\n')
    excerpt = ''
    line = caller.line - 1
    col = caller.char
    offset = 50
    needle = 'toDataURL'
    for i in [-1, 0, 1]:
        if 0 <= line + i < len(by_line):
            text = by_line[line + i][max(col - offset // 3, 0): col + offset + len(needle)]
            if needle in text:
                excerpt = text
                break
    script['excerpt'] = excerpt
    script['obfuscated'] = not any([
        find_text_in_region(needle, body, caller.line - 1, caller.char, offset),
        find_text_in_region(needle, body, caller.line, caller.char, offset),
        find_text_in_region(needle, body, caller.line + 1, caller.char, offset),
    ])

    obj = redis_conn.get(identifier)
    if obj is not None:
        entry = json.loads(redis_conn.get(identifier).decode('utf8'))
        if 'emoji' not in body:
            entry['scripts'].append(script)
            redis_conn.set(identifier, json.dumps(entry))


COOKIE_NAME = '_id'


@route('/start/<identifier>', ['GET'])
def start(identifier):
    response.set_cookie(COOKIE_NAME, identifier, path='/')
    return ''


@route('/', ['GET', 'POST', 'OPTIONS'])
def log():
    if request.method == 'POST':
        args = request.json
        identifier = request.get_cookie(COOKIE_NAME)
        Process(target=process_log_entry, args=(identifier, args,)).start()

    response.set_header('Access-Control-Allow-Origin', request.get_header('Origin', '*'))
    response.set_header('Access-Control-Max-Age', '1728000')
    response.set_header('Access-Control-Allow-Credentials', 'true')
    response.set_header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS')
    response.set_header('Access-Control-Allow-Headers',
                        'User-Agent,Cookie,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type')
    return ''


def main():
    run(host=config.HOST, port=config.PORT, server='gunicorn', workers=4)


if __name__ == '__main__':
    main()
