from sqlite3 import Connection as SQLiteConnection

from io import BytesIO
from sqlalchemy import create_engine, Column, Integer, Unicode, ForeignKey, DateTime, Boolean, String
from sqlalchemy.event import listens_for
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import scoped_session, sessionmaker

import config
from server.utils import (utc_now, download_file, extract_domain, analyze_js_stacktrace, StackTrace,
                          tlsh_func, find_text_in_region, server_logger)

engine = create_engine(config.SQLALCHEMY_DATABASE_URI, convert_unicode=True)
Session = scoped_session(sessionmaker(autocommit=False, autoflush=False, bind=engine))

Base = declarative_base()
Base.query = Session.query_property()


@listens_for(engine, "connect")
def _set_sqlite_pragma(dbapi_conn, connection_record):
    if isinstance(dbapi_conn, SQLiteConnection):
        dbapi_conn.create_function('domain', 1, extract_domain)
        dbapi_conn.execute("PRAGMA foreign_keys=ON;")


def init_db():
    Base.metadata.create_all(bind=engine)

    sites = config.load_alexa_csv(config.ALEXA_FILE_PATH)[:20000]
    if not Domain.query.get(0):
        Session.add(Domain(id=0, domain='unknown', alexa_rank=-1))
        Session.commit()

    domains = set(domain for domain, in Domain.query.with_entities(Domain.domain).all())
    not_inserted = set(sites) - domains
    for i, domain in enumerate(sites, 1):
        if domain in not_inserted:
            Session.add(Domain(alexa_rank=i, domain=domain))
        if i % 10000 == 0:
            # To clear the queue from time to time
            Session.commit()
    Session.commit()


class Domain(Base):
    __tablename__ = 'domain'
    id = Column(Integer, primary_key=True)
    domain = Column(Unicode, unique=True, nullable=False)
    alexa_rank = Column(Integer, index=True, default=-1, nullable=False)

    @classmethod
    def rank_subset(cls, rank_start=1, rank_end=None):
        q = cls.query.filter(cls.alexa_rank > rank_start)
        if rank_end is not None:
            q = q.filter(cls.alexa_rank < rank_end)

        return {domain.id: domain for domain in q.all()}


class Log(Base):
    __tablename__ = 'log'
    id = Column(Integer, primary_key=True)
    domain_id = Column(Integer, ForeignKey(Domain.id), nullable=False)
    measured_at = Column(DateTime(timezone=True), default=utc_now)

    canvas_width = Column(Integer)
    canvas_height = Column(Integer)

    referrer = Column(Unicode)
    source_url = Column(Unicode)
    source_html = Column(String)
    source_tlsh = Column(Unicode)

    stacktrace = Column(Unicode)

    # Probably could be normalized but this way is easier to query
    # when having only the database
    st_caller_file = Column(Unicode)
    st_caller_line = Column(Integer)
    st_caller_char = Column(Integer)
    st_caller_tlsh = Column(Unicode)

    st_init_file = Column(Unicode)
    st_init_line = Column(Integer)
    st_init_char = Column(Integer)
    st_init_tlsh = Column(Unicode)

    is_obfuscated = Column(Boolean)

    @property
    def caller_hostname(self):
        return extract_domain(self.st_caller_file)

    @property
    def st_caller(self):
        return StackTrace(None, self.st_caller_file, self.st_caller_line, self.st_caller_char)

    @property
    def st_init(self):
        return StackTrace(None, self.st_init_file, self.st_init_line, self.st_init_char)

    # This could be extracted to an insert trigger or an hybrid setter for sqlalchemy
    def set_stacktrace(self, stacktrace):
        self.stacktrace = stacktrace

        caller, initiator = analyze_js_stacktrace(stacktrace)
        self.st_caller_file = caller.file
        self.st_caller_line = caller.line
        self.st_caller_char = caller.char

        self.st_init_file = initiator.file
        self.st_init_line = initiator.line
        self.st_init_char = initiator.char

    def check_obfuscation(self):
        if self.st_caller_file == self.source_url:
            body = self.source_html
            self.st_caller_tlsh = self.source_tlsh
        else:
            body = download_file(self.st_caller_file, BytesIO()).read()
            self.st_caller_tlsh = tlsh_func(body)

        if self.st_caller_file == self.st_init_file:
            self.st_init_tlsh = self.st_caller_tlsh
        else:
            self.st_init_tlsh = tlsh_func(download_file(self.st_init_file, BytesIO()).read())

        try:
            body = body.decode('utf8')
        except UnicodeEncodeError:
            RuntimeWarning('Bad file downloaded')
            return False

        server_logger.debug('searching in %s' % (self.st_caller,))
        self.is_obfuscated = not any([
            find_text_in_region('toDataURL', body, self.st_caller_line - 1, self.st_caller_char, 50),
            find_text_in_region('toDataURL', body, self.st_caller_line, self.st_caller_char, 50),
            find_text_in_region('toDataURL', body, self.st_caller_line + 1, self.st_caller_char, 50),
        ])

        return self.is_obfuscated
