import html
from multiprocessing import Process

import os
from bottle import BaseRequest, route, run, request, response, static_file
from sqlalchemy import or_

import config
from server.models import Domain, Log, init_db, Session
from server.utils import extract_domain, tlsh_func, server_logger

BaseRequest.MEMFILE_MAX = 2 << 20  # 2 MB should be enough


def process_log_entry(args):
    referrer = args.get('referrer')
    source_url = args.get('src')
    source_html = args.get('doc').encode('utf8')
    stacktrace = args.get('stack')
    width = args.get('w')
    height = args.get('h')

    origin_url = referrer if referrer else source_url
    domain = extract_domain(referrer or source_url)
    # Some pages add the www and we check both just in case
    domain_www = domain.split('.', 1)[1]
    domain = Domain.query.filter(or_(
        Domain.domain == domain,
        Domain.domain == domain_www
    )).first()

    domain_id = 0 if domain is None else domain.id
    if domain_id == 0:
        server_logger.warning('domain not found: %s [ref:%s src:%s]' % (domain, referrer, source_url))

    log_entry = Log(
        domain_id=domain_id,
        canvas_width=width,
        canvas_height=height,
        referrer=referrer,
        source_url=source_url,
        # TODO: There's really no point into preserving the encoded version.
        # We already know it's utf8 compatible after this
        source_html=html.unescape(source_html.decode('utf8')).encode('utf8'),
        source_tlsh=tlsh_func(source_html),
    )
    log_entry.set_stacktrace(stacktrace)
    log_entry.check_obfuscation()
    Session.add(log_entry)
    Session.commit()
    Session.remove()


@route('/', ['GET', 'POST', 'OPTIONS'])
def log():
    if request.method == 'POST':
        args = request.json
        Process(target=process_log_entry, args=(args,)).start()

    response.set_header('Access-Control-Allow-Origin', request.get_header('Origin', '*'))
    response.set_header('Access-Control-Max-Age', '1728000')
    response.set_header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS')
    response.set_header('Access-Control-Allow-Headers',
                        'User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type')
    return ''


TEST_DIR = os.path.join(config.PROJECT_DIR, 'server/test')


@route('/test/<filename>')
def test_pages(filename):
    return static_file(filename, root=TEST_DIR)


def main():
    init_db()
    run(host=config.HOST, port=config.PORT, server='gunicorn', workers=4)


if __name__ == '__main__':
    main()
