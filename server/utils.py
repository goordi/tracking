import logging
from datetime import datetime, timezone
from urllib.parse import urlparse

import requests
from collections import namedtuple, defaultdict
from hashlib import sha256

try:
    import tlsh

    tlsh_func = tlsh.hash
except ImportError:
    RuntimeWarning('You will have to install tlsh python extension manually'
                   ' (https://github.com/trendmicro/tlsh) to get local space hashing functionality')
    tlsh_func = lambda x: ''

server_logger = logging.getLogger('server')
handler = logging.FileHandler('server.log')
handler.setLevel(logging.DEBUG)
handler.setFormatter(logging.Formatter('[%(asctime)s] %(levelname)s\n\t%(message)s'))
server_logger.setLevel(logging.DEBUG)
server_logger.addHandler(handler)


def utc_now():
    return datetime.now(timezone.utc)


def extract_domain(url):
    return urlparse(url).hostname


def download_file(url, destination, headers=None):
    h = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:45.0) Gecko/20100101 Firefox/45.0'}
    if headers is not None:
        h.update(headers)

    resp = requests.get(url, stream=True, headers=h)
    for chunk in resp.iter_content(chunk_size=4096):
        if chunk:
            destination.write(chunk)

    destination.seek(0)
    return destination


def hash_file(f, hash_func=sha256):
    h = hash_func()
    for chunk in iter(lambda: f.read(4096), b""):
        h.update(chunk)

    return h.hexdigest()


def lsh_file(f):
    return tlsh_func(f.read()).lower()


StackTrace = namedtuple('StackTrace', 'function, file, line, char')


def _make_stacktrace(line):
    func, rest = line.split('@', 1)
    file, line, char = rest.rsplit(':', 2)
    return StackTrace(func, file, int(line), int(char))


def analyze_js_stacktrace(stacktrace):
    calls = [line for line in stacktrace.split('\n') if line]
    # calls 0 is our injected function
    caller, initiator = calls[1], calls[-1]
    return _make_stacktrace(caller), _make_stacktrace(initiator)


def find_text_in_region(needle, haystack, line, col, offset=0):
    if len(haystack) == 0:
        server_logger.warning('haystack was empty')
    haystack = haystack.split('\n')
    # line is 1 indexed so
    line -= 1
    if 0 <= line < len(haystack):
        excerpt = haystack[line][max(col - offset // 3, 0): col + offset + len(needle)]
        if excerpt:
            server_logger.debug('excerpt: %s' % excerpt)
        return needle in excerpt
    return False

def groupby(list_of_objects, func, thresh):
    groups = defaultdict(list)
    visited = set()
    set_of_objects = set(list_of_objects)
    while len(visited) < len(set_of_objects):
        current = None
        for item in set(set_of_objects) - visited:
            if not current:
                current = item
                visited.add(current)
            else:
                if func(item, current) < thresh:
                    visited.add(item)
                    groups[current].append(item)
    return groups
