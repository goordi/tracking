var scriptNode = document.createElement('script');
/*
 This function replaces the global HTMLCanvasElement.prototype.toDataURL
 with our own logging version.

 We take a snapshot of the full page to get injected html
 */
function instrument() {
    var old = HTMLCanvasElement.prototype.toDataURL;
    HTMLCanvasElement.prototype.toDataURL = function(c) {
		var trace = (new Error).stack;
		var xhr = new XMLHttpRequest();
		xhr.open("POST", "http://localhost:9000/", true);
		xhr.withCredentials = true;
		xhr.setRequestHeader("Content-Type", "application/json");
		var params = {
			w: this.scrollWidth,
			h: this.scrollHeight,
			referrer: document.referrer,
			src: window.location.href,
			stack: trace,
			doc: new XMLSerializer().serializeToString(document),
		}
		xhr.send(JSON.stringify(params));
		return old.apply(this, arguments);
	}
	var self = document.currentScript;
	self.parentNode.removeChild(self);
}

scriptNode.innerHTML = '('+instrument.toString()+')();';
where = document.head || document.body;
if (where) {
    where.insertBefore(scriptNode, where.firstChild);
}
